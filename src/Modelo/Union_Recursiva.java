/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author JaiderO
 */
public class Union_Recursiva {

    public Union_Recursiva() {
    }

    public int[] union(int[] a1, int[] a2){
        
        if(a1.length == 0 && a2.length == 0){
            throw new RuntimeException("Error, ambos vectores no pueden ser vacios");
        }
        if(a1.length == 0){
            return a2;
        }
        if(a2.length == 0){
            return a1;
        }
        
        int[] a3 = new int[a1.length + a2.length];
        
        int i = 0;
        unir(a3,a1,0,i);
        
        i += a1.length;
        unir(a3,a2,0,i);
        
        int posVacias = this.posVacias(a3);
        
        if(posVacias > 0){
            return this.redimensionar(a3, posVacias);
        }
        
        return a3;
    }
    
    
    private void unir(int[] union, int[] unir, int indice, int i){
        
        if(indice < unir.length){
            
            if(!this.decoSearch(unir[indice], union, 0)){
                union[i++] = unir[indice];
            }
            unir(union,unir,indice+1,i);
            
        }
        
    }
    
    
    public boolean decoSearch(int x,int []v,int indice){
        
        if(v.length == 0){
            throw new RuntimeException("Error, el vector no puede estar vacio");
        }
        if(indice != 0){
            return search(x,v,indice*0);
        }
        return search(x,v,indice);
    }
    
    public boolean search(int x,int []v,int indice){
        
        if(indice == v.length){
            return false;
        }
        if(v[indice] == x){
            return true;
        }
        return search(x,v,indice+1);
        
    }
    
    public int posVacias(int [] a){
        
        int posNoValidas = 0;
        
        for (int i = 0; i < a.length; i++) {
            
            if(a[i] == 0){
                posNoValidas += 1;
            }
            
        }
        
        return posNoValidas;
    }
    
    private int[] redimensionar(int[] a, int posVacias){
    
        int [] b = new int[a.length - posVacias];
        
        for (int i = 0; i < b.length; i++) {
            b[i] = a[i];
        }
        
        return b;
        
    }
    
}
