/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Union_Recursiva;

/**
 *
 * @author JaiderO
 */
public class Prueba {
    
    public static void main(String[] args) {
        
        Union_Recursiva r = new Union_Recursiva();
        
        int[] a = {1,2,3,4,5};
        int[] b = {6,6,8,9,10};
        
        int[] c = r.union(a, b);
        
        
        for (int i = 0; i < c.length; i++) {
            
            System.out.println(c[i]);
            
        }
        
    }
    
}
